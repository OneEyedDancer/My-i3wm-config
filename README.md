# I3wm | Arch Linux
![icon](https://codeberg.org/OneEyedDancer/My-i3wm-config/raw/branch/master/OYD-i3wm/screenshot1.png)
### Programs
| category       | name                                |
|----------------|:-----------------------------------:|
| window manager | i3-gapps                            |
| bar            | polybar                             |
| launcher       | rofi                                |
| notifications  | dunst                               |
| composer       | picom                               |
| browser        | librewolf(Devoud on screenshot)     |
| terminal       | kitty                               |
| file manager   | nemo                                |
| wallpapers     | nitrogen                            |
| theme          | lxappearance-gtk3(Mint-Y-Dark-Teal) |
| icons          | Papirus-Dark                        |
| cursor         | Volantes Light Cursors              |
| music player   | amberol                             |
| video player   | mpv                                 |
| screenshots    | flameshot                           |
| text editor    | xed                                 |
| bluetooth      | blueberry                           |

### Installation command(AUR helper)
```
paru -S i3-gapps polybar rofi dunst picom nemo kitty nitrogen lxappearance-gtk3 amberol papirus-icon-theme papirus-folders blueberry flameshot xed mpv librewolf
```
### or 
```
yay -S i3-gapps polybar rofi dunst picom nemo kitty nitrogen lxappearance-gtk3 amberol papirus-icon-theme papirus-folders blueberry flameshot xed mpv librewolf
```
## Place of configurations
```
i3 -> ~/.config/
picom.conf -> ~/.config/
kitty -> ~/.config/
```
---
![Screenshot3](https://codeberg.org/OneEyedDancer/My-i3wm-config/raw/branch/master/OYD-i3wm/screenshot2.png)
